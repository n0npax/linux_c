/*
 ============================================================================
 Author      : n0npax
 Version     :
 Copyright   : GPL v3
 DESC		 : mount file in the stack
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

struct stat my_stat;
int fd;

//open file and prepare stat struct.
void prepare_stat(char *argv[], int *fd_ptr) {
	fd = open(argv[1], O_RDONLY);

	if (*fd_ptr < 0) {
		perror("open error");
		exit(1);
	}
	if (fstat(*fd_ptr, &my_stat) < 0) {
		perror("fstat error");
		exit(1);
	}
	if (!S_ISREG(my_stat.st_mode)) {
		perror("fstat not regular file");
		exit(1);
	}
}

//map file in the stack and return pointer
char *map_file(int fd) {
	char *p = mmap(0, my_stat.st_size, PROT_READ, MAP_SHARED, fd, 0);

	if (p == MAP_FAILED) {
		perror("map failed");
		exit(1);
	}
	if (close(fd) < 0) {
		perror("close descriptor failed");
		exit(1);
	}
	return p;
}

//int main(int argc, char *argv[]) {
int main_fake(int argc, char *argv[]) {

	if (argc != 2) {
		perror("need file as parameter");
		exit(1);
	}

	prepare_stat(argv, &fd);
	char *p = map_file(fd);

	for (int i = 0; i < my_stat.st_size; ++i) {
		printf("%c", p[i]);
	}

	if (munmap(p, my_stat.st_size)) {
		perror("unmap error");
		exit(1);
	}

	return EXIT_SUCCESS;
}
