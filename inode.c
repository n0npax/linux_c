/*
 ============================================================================
 Author      : n0npax
 Version     :
 Copyright   : GPL v3
 DESC		 : show file addr
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <errno.h>

int fd;

//get phisical addr on dics
int get_blk(int logical_blk) {
	int ret = ioctl(fd, FIBMAP, &logical_blk);

	if (ret < 0) {
		perror("ioctl");
		exit(1);
	}
	return logical_blk;
}

//get blocks len
int get_blks_nr(void) {
	struct stat buff;
	if (fstat(fd, &buff) < 0) {
		perror("fstat");
		exit(1);
	}
	return buff.st_blocks;
}

//get inode
int get_inode() {
	struct stat buff;
	if (fstat(fd, &buff) < 0) {
		perror("fstat");
		exit(1);
	}
	return buff.st_ino;
}

//print infos
void print_blocks(void) {
	int nr_blks = get_blks_nr();

	printf("[indoe: %i]\n", get_inode());
	printf("<block: %i>\n", nr_blks);

	for (int i = 0; i < nr_blks; i++) {
		int phi_blk = get_blk(i);
		if (!phi_blk) {
			continue;
		}
		printf("(%i %i) ", i, phi_blk);
	}
	printf("\n");
}

int main(int argc, char *argv[]) {
//int main_fake(int argc, char *argv[]) {
	if (getuid()) {
		perror("use as a root");
		exit(1);
	}
	if (argc != 2) {
		perror("need file as param!");
		exit(1);
	}

	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror("file open error");
		exit(1);
	}

	print_blocks();
	return EXIT_SUCCESS;
}
